Jawaban Tugas 10 - Belajar SQL
PKS Digital School
-----------------------------------------
1. Buat Database
   #syntax :
	create database digital_shop;

2. Buat Table

   - masuk ke database yg kita bikin :
     #syntax :
	use digital_shop;

   - Membuat Table Users :
     #syntax :
	create table users(
    -> id int(9) primary key auto_increment,
    -> nama varchar(100),
    -> email varchar(100),
    -> password varchar(100)
    -> );


   - Membuat Table Cateories :
     #syntax :
	create table categories(
    -> id int(9) primary key auto_increment,
    -> nama varchar(150)
    -> );

   - Membuat Table Items :
     #syntax :
	create table items(
    -> id int(9) primary key auto_increment,
    -> nama varchar(200),
    -> description varchar(255),
    -> price int(50),
    -> stock int(50),
    -> category_id int(9),
    -> foreign key(category_id) references categories(id)
    -> );

3. Masukin Data

   - Memasukkan data users :
     #syntax :
	insert into users(nama, email, password) values
    -> ("Budi Brother", "budibrt03@gmail.com", "budi03"),
    -> ("Rangga Guyon", "rangga_gyn@gmail.com", "guyon55"),
    -> ("silvia agustina", "slvia.agustna@gmail.com", "silviakuy"),
    -> ("Shophia Aulia", "shophia.au@gmail.com", "aulia332"),
    -> ("Joni Bulan Bintang", "joni.dunia@gmail.com", "duniajoni123");


   - Memasukkan data Cateories :
     #syntax :
	insert into categories(nama) values("Aplikasi Android"), ("Aplikasi Komputer"), ("Aplikasi Multi Device");

   - Memasukkan data Items :
     #syntax :
	insert into items(nama, description, price, stock, category_id) values
    -> ("Zoom Premium", "Adalah layanan zoom premium tanpa batas waktu", 34000, 20, 3),
    -> ("Youtube Premium", "Adalah layanan Youtube premium tanpa Iklan", 12000, 40, 3),
    -> ("Corel Draw 2020 Pro", "Adalah aplikasi desain grafis di komputer", 56000, 25, 2),
    -> ("Canva Pro", "Adalah aplikasi desain grafis berbasis drag and drop", 15000, 59, 3),
    -> ("Aligh Motion Pro", "Adalah aplikasi editor photo di android", 36000, 14, 1),
    -> ("Iflix VIP", "Adalah layanan menonton film ", 24000, 32, 3);

4. Mengambil Data

     - Menampilkan semua data User kecuali password :
     #syntax :
	select id, nama, email from users;


   - Menampilkan Data diatas harga 24.000 :
     #syntax :
	select * from items where price > 24000;

    - Menampilkan Data yang filter dengan nama barang "premium" :
     #syntax :
	select * from items where nama like '%premium';

    - Menampilkan Data items join dengan categories :
     #syntax :
	select items.id, items.nama, items.price, items.stock, items.category_id, categories.nama from items inner join categories on items.category_id = categories.id;


5. Update Data

   - Mengubah nama users :
     #syntax :
	update users set nama = "Joni Dunia" where id=5;


                   ----------- Sekian dan Terimakasih ----------